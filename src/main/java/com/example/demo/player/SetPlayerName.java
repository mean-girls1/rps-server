package com.example.demo.player;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;


@Data
@NoArgsConstructor
public class SetPlayerName {
    String name;
}
