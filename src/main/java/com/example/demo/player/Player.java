package com.example.demo.player;

import lombok.Value;

@Value
public class Player {
    String token;
    String name;
    String gameOwner;
    String joinedGame;
}
