package com.example.demo.player;

import com.example.demo.gameStatus.GameEntity;
import lombok.*;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity @Table(name = "player")
public class PlayerEntity {
    @Id String token;
    @Column(name = "name") String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "game_owner_id", referencedColumnName = "id") GameEntity gameOwner;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "joined_game_id", referencedColumnName = "id") GameEntity joinedGame;

    public PlayerEntity(String token) {
        this.token = token;
    }
}
