package com.example.demo.gameStatus;

import com.example.demo.player.PlayerEntity;
import lombok.*;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "game")
public class GameEntity {
    @Id String id;

    @OneToOne(mappedBy = "gameOwner")
    PlayerEntity player;

    @Enumerated(EnumType.STRING)
    @Column(name = "move", columnDefinition = "ENUM('ROCK', 'PAPER', 'SCISSORS')")
    Move move;

    @Enumerated(EnumType.STRING)
    @Column(name = "game_status", columnDefinition = "ENUM('NONE', 'OPEN', 'ACTIVE', 'WIN', 'LOSE', 'DRAW')")
    GameStatus gameStatus;

    @OneToOne(mappedBy = "joinedGame")
    PlayerEntity opponent;

    @Enumerated(EnumType.STRING)
    @Column(name = "opponent_move", columnDefinition = "ENUM('ROCK', 'PAPER', 'SCISSORS')")
    Move opponentMove;

    public GameEntity(String id) {
        this.id = id;
    }

}
