package com.example.demo.gameStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class Game {
    String id;
    String name;
    String move;
    String gameStatus;
    String opponentName;
    String opponentMove;
}

enum Move{
    ROCK,
    PAPER,
    SCISSORS
}

enum GameStatus{
    NONE,
    OPEN,
    ACTIVE,
    WIN,
    LOSE,
    DRAW
}