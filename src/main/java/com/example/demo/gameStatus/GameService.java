package com.example.demo.gameStatus;

import com.example.demo.exceptions.Activity;
import com.example.demo.exceptions.ExceptionType;
import com.example.demo.exceptions.UseException;
import com.example.demo.player.PlayerEntity;
import com.example.demo.player.PlayerRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class GameService {
    PlayerRepository playerRepository;
    GameRepository gameRepository;

    public GameService(PlayerRepository playerRepository, GameRepository gameRepository) {
        this.playerRepository = playerRepository;
        this.gameRepository = gameRepository;
    }

    public PlayerEntity createNewPlayer() {
        PlayerEntity playerEntity = new PlayerEntity(UUID.randomUUID().toString());
        playerRepository.save(playerEntity);
        return playerEntity;
    }

    public Optional<PlayerEntity> setName(String token, String name) {
        return getPlayerByToken(token)
                .map(playerEntity -> {
                    playerEntity.setName(name);
                    return playerRepository.save(playerEntity);
                });
    }

    public Optional<PlayerEntity> getPlayerByToken(String token) {
        return playerRepository.findById(token);
    }

    public Optional<GameEntity> startNewGame(String token) throws UseException {
        GameEntity gameEntity = new GameEntity(UUID.randomUUID().toString());
        PlayerEntity playerEntity = getPlayerEntity(token);
        setStartedGameValues(gameEntity, playerEntity);
        playerRepository.save(playerEntity);
        gameRepository.save(gameEntity);
        return Optional.of(gameEntity);
    }

    public Optional<GameEntity> joinGame(String token, String gameId) throws UseException {
        PlayerEntity playerEntity = getPlayerEntity(token);
        GameEntity gameEntity = gameRepository.findById(gameId).orElseThrow(() ->
                new UseException(Activity.JOIN_GAME, ExceptionType.GAME_ID_NOT_FOUND));
        setJoinedGameValues(gameEntity, playerEntity);
        playerRepository.save(playerEntity);
        gameRepository.save(gameEntity);
        return Optional.of(gameEntity);
    }

    public Optional<GameEntity> getGameStatusByToken(String token) throws UseException {
        PlayerEntity playerEntity = getPlayerEntity(token);
        return Optional.of(playerEntity.getGameOwner() == null ?
                playerEntity.getJoinedGame() : playerEntity.getGameOwner());
    }

    public Stream<GameEntity> getGameList() {
        return gameRepository.findAll().stream().filter(gameEntity ->
                gameEntity.getGameStatus().equals(GameStatus.OPEN));
    }

    public Optional<GameEntity> makeMove(String token, String sign) throws UseException {
        Move move = handleMove(sign);
        PlayerEntity playerEntity = getPlayerEntity(token);
        GameEntity gameEntity = getCorrectGame(playerEntity);
        setCorrectPlayerMove(move, playerEntity, gameEntity);
        if (gameEntity.getMove() != null && gameEntity.getOpponentMove() != null) {
            gameEntity.setGameStatus(calculateGameResult(gameEntity.getMove(), gameEntity.getOpponentMove()));
        }
        return Optional.of(gameRepository.save(gameEntity));
    }

    private Move handleMove(String sign) throws UseException {
        try {
            return Move.valueOf(sign.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new UseException(Activity.MAKE_MOVE, ExceptionType.NOT_VALID);
        }
    }

    private GameStatus calculateGameResult(Move mainMove, Move opponentMove) throws UseException {
        switch (mainMove) {
            case ROCK: return opponentMove == Move.PAPER ? GameStatus.LOSE : opponentMove == Move.SCISSORS ? GameStatus.WIN : GameStatus.DRAW;
            case PAPER: return opponentMove == Move.SCISSORS ? GameStatus.LOSE : opponentMove == Move.ROCK ? GameStatus.WIN : GameStatus.DRAW;
            case SCISSORS: return opponentMove == Move.ROCK ? GameStatus.LOSE : opponentMove == Move.PAPER ? GameStatus.WIN: GameStatus.DRAW;
            default: throw new UseException(Activity.MAKE_MOVE, ExceptionType.NOT_VALID);
        }
    }

    public Optional<GameEntity> getGameById(String gameId) {
        return gameRepository.findById(gameId);
    }

    private GameEntity getCorrectGame(PlayerEntity playerEntity) throws UseException {
        Optional<GameEntity> gameEntity = playerEntity.getGameOwner() == null ?
                gameRepository.findById(playerEntity.getJoinedGame().getId()) :
                gameRepository.findById(playerEntity.getGameOwner().getId());
        return gameEntity.orElseThrow(() -> new UseException(Activity.FIND_GAME, ExceptionType.NOT_FOUND));
    }

    public PlayerEntity getPlayerEntity(String token) throws UseException {
        return getPlayerByToken(token)
                .orElseThrow(() -> new UseException(Activity.GET_PLAYER, ExceptionType.NOT_FOUND));
    }

    private void setStartedGameValues(GameEntity gameEntity, PlayerEntity playerEntity) {
        gameEntity.setPlayer(playerEntity);
        gameEntity.setGameStatus(GameStatus.OPEN);
        playerEntity.setGameOwner(gameEntity);
    }

    private void setJoinedGameValues(GameEntity gameEntity, PlayerEntity playerEntity) {
        gameEntity.setOpponent(playerEntity);
        gameEntity.setGameStatus(GameStatus.ACTIVE);
        playerEntity.setJoinedGame(gameEntity);
    }

    private void setCorrectPlayerMove(Move move, PlayerEntity playerEntity, GameEntity gameEntity) {
        if (playerEntity.getGameOwner() == null) {
            gameEntity.setOpponentMove(move);
        } else {
            gameEntity.setMove(move);
        }
    }
}
