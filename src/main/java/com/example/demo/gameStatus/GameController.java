package com.example.demo.gameStatus;

import com.example.demo.exceptions.Activity;
import com.example.demo.exceptions.ExceptionType;
import com.example.demo.exceptions.UseException;
import com.example.demo.player.Player;
import com.example.demo.player.PlayerEntity;
import com.example.demo.player.SetPlayerName;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GameController {
    GameService gameService;

    @GetMapping("/auth/token")
    public String getToken() {
        return entityToDTO(gameService.createNewPlayer()).getToken();
    }

    @PostMapping("/user/name")
    public ResponseEntity<Player> setName(@RequestHeader("token") String token,
                                          @RequestBody SetPlayerName name) throws UseException {
        return gameService.setName(token, name.getName())
                .map(this::entityToDTO)
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new UseException(Activity.CREATE_PLAYER, ExceptionType.TOKEN_NOT_FOUND));
    }

    @GetMapping("/games/start")
    public Game startGame(@RequestHeader("token") String token) throws UseException {
        PlayerEntity playerEntity = gameService.getPlayerEntity(token);
        return gameService.startNewGame(token)
                .map(gameEntity -> entityToDTO(gameEntity, playerEntity))
                .orElseThrow(() -> new UseException(Activity.START_GAME, ExceptionType.TOKEN_NOT_FOUND));
    }

    @GetMapping("/games/join/{gameId}")
    public Game joinGame(@RequestHeader("token") String token,
                         @PathVariable("gameId") String gameId) throws UseException {
        PlayerEntity playerEntity = gameService.getPlayerEntity(token);
        return gameService.joinGame(token, gameId)
                .map(gameEntity -> entityToDTO(gameEntity, playerEntity))
                .orElseThrow(() -> new UseException(Activity.JOIN_GAME, ExceptionType.GAME_ID_NOT_FOUND));
    }

    @GetMapping("/games/status")
    public Game gameStatus(@RequestHeader("token") String token) throws UseException {
        PlayerEntity playerEntity = gameService.getPlayerEntity(token);
        return gameService.getGameStatusByToken(token)
                .map(gameEntity -> entityToDTO(gameEntity, playerEntity))
                .orElseThrow(() -> new UseException(Activity.FIND_GAME, ExceptionType.TOKEN_NOT_FOUND));
    }

    @GetMapping("/games")
    public List<Game> gameList(@RequestHeader("token") String token) throws UseException {
        PlayerEntity playerEntity = gameService.getPlayerEntity(token);
        return gameService.getGameList()
                .map(gameEntity -> entityToDTO(gameEntity, playerEntity))
                .collect(Collectors.toList());
    }

    @GetMapping("/games/move")
    public Game makeMove(@RequestHeader("token") String token,
                         @RequestParam(value = "sign") String sign) throws UseException {
        PlayerEntity playerEntity = gameService.getPlayerEntity(token);
        return gameService.makeMove(token, sign)
                .map(gameEntity -> entityToDTO(gameEntity, playerEntity))
                .orElseThrow(() -> new UseException(Activity.MAKE_MOVE, ExceptionType.GAME_ID_NOT_FOUND));
    }

    @GetMapping("/games/{id}")
    public Game getGameInfo(@RequestHeader(value = "token") String token,
                            @PathVariable("id") String gameId) throws UseException {
        PlayerEntity playerEntity = gameService.getPlayerEntity(token);
        return gameService.getGameById(gameId)
                .map(gameEntity -> entityToDTO(gameEntity, playerEntity))
                .orElseThrow(() -> new UseException(Activity.FIND_GAME, ExceptionType.GAME_ID_NOT_FOUND));
    }

    private Player entityToDTO(PlayerEntity playerEntity) {
        return new Player(playerEntity.getToken(),
                playerEntity.getName(),
                playerEntity.getGameOwner() != null ? playerEntity.getGameOwner().getId() : null,
                playerEntity.getJoinedGame() != null ? playerEntity.getJoinedGame().getId() : null);
    }

    private Game entityToDTO(GameEntity gameEntity, PlayerEntity playerEntity) {
        if (playerEntity.getJoinedGame() != null) {
            return getCorrectGame(gameEntity,
                    gameEntity.getOpponent(),
                    gameEntity.getOpponentMove(),
                    calculateOpponentGameStatus(gameEntity.getGameStatus()),
                    gameEntity.getPlayer(),
                    gameEntity.getMove());
        }
        return getCorrectGame(gameEntity,
                gameEntity.getPlayer(),
                gameEntity.getMove(),
                gameEntity.getGameStatus(),
                gameEntity.getOpponent(),
                gameEntity.getOpponentMove());
    }

    private Game getCorrectGame(GameEntity gameEntity, PlayerEntity player, Move move, GameStatus gameStatus, PlayerEntity opponent, Move opponentMove) {
        return new Game(gameEntity.getId(),
                player != null ? player.getName() : null,
                move != null ? move.name() : null,
                gameStatus.name(),
                opponent != null ? opponent.getName() : null,
                opponentMove != null ? opponentMove.name() : null);
    }

    private GameStatus calculateOpponentGameStatus(GameStatus gameStatus) {
        switch (gameStatus) {
            case WIN:
                return GameStatus.LOSE;
            case LOSE:
                return GameStatus.WIN;
            default:
                return gameStatus;
        }
    }
}