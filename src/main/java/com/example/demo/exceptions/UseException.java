package com.example.demo.exceptions;

public class UseException extends Exception{
    Activity activity;
    ExceptionType exceptionType;
    Object[] params;

    public UseException(Activity activity, ExceptionType exceptionType, Object... params) {
        super(activity+" failed because "+ exceptionType);
        this.activity = activity;
        this.exceptionType = exceptionType;
        this.params = params;
    }

    public Activity getActivity() {
        return activity;
    }

    public ExceptionType getUserExceptionType() {
        return exceptionType;
    }

    public Object[] getParams() {
        return params;
    }
}
