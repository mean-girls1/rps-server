package com.example.demo.exceptions;

public enum ExceptionType {
    NOT_FOUND,
    NOT_VALID,
    TOKEN_NOT_FOUND,
    GAME_ID_NOT_FOUND
}
