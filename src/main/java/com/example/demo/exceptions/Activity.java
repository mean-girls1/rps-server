package com.example.demo.exceptions;

public enum Activity {
    START_GAME,
    CREATE_PLAYER,
    GET_PLAYER,
    FIND_GAME,
    JOIN_GAME,
    MAKE_MOVE
}
