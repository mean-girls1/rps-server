CREATE TABLE game (
     id VARCHAR(40),
     move ENUM('ROCK', 'PAPER', 'SCISSORS'),
     game_status ENUM('NONE', 'OPEN', 'ACTIVE', 'WIN', 'LOSE', 'DRAW'),
     opponent_move ENUM('ROCK', 'PAPER', 'SCISSORS'),
     PRIMARY KEY(id)
 );

CREATE TABLE player (
    token VARCHAR(40),
    name VARCHAR(30),
    game_owner_id VARCHAR(40),
    joined_game_id VARCHAR(40)
)