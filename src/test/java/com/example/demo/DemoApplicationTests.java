//package com.example.demo;
//
//import com.example.demo.exceptions.UseException;
//import com.example.demo.gameStatus.Game;
//import com.example.demo.gameStatus.GameController;
//import com.example.demo.gameStatus.GameRepository;
//import com.example.demo.player.Player;
//import com.example.demo.player.PlayerRepository;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.ActiveProfiles;
//
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@ActiveProfiles("integrationtest")
//class DemoApplicationTests {
//
//    @Autowired
//    PlayerRepository playerRepository;
//    @Autowired
//    GameRepository gameRepository;
//    @Autowired
//    GameController gameController;
//
//    @BeforeEach
//    void setUp() {
//        playerRepository.deleteAll();
//        gameRepository.deleteAll();
//    }
//
//
//    @Test
//    void get_token() {
//        //Given
//        Player token = gameController.getToken();
//
//        //Then
//        assertNotNull(token.getToken());
//        System.out.println("Token" + token + " is given!");
//    }
//
//    @Test
//    void start_game_success() throws UseException {
//        //Given
//        Player token = gameController.getToken();
//
//        //When
//        Game game = gameController.startGame(token.getToken());
//
//        //Then
//        assertEquals("OPEN", game.getGameStatus());
//        System.out.println("Game " + game.getGameStatus() + " is started!");
//    }
//
//    @Test
//    void set_name_success() throws UseException {
//        //Given
//        Player token = gameController.getToken();
//
//        //When
//        ResponseEntity<Player> player = gameController.setName(token.getToken(), "majs");
//
//        //Then
//        assertEquals(200, player.getStatusCodeValue());
//        System.out.println("Name: " + player.getBody() + " is named!");
//    }
//
//    @Test
//    void join_game_success() throws UseException {
//        //Given
//        Player token = gameController.getToken();
//        Player token2 = gameController.getToken();
//        gameController.setName(token.getToken(), "Majs");
//        gameController.setName(token2.getToken(), "Kolv");
//        Game game = gameController.startGame(token.getToken());
//
//        //When
//       game = gameController.joinGame(token2.getToken(), game.getId());
//
//        //Then
//        assertEquals("ACTIVE", game.getGameStatus());
//        System.out.println(game.getName()+ "´s game is joined by " + game.getOpponentName());
//
//    }
//
//    @Test
//    void check_game_status_success() throws UseException {
//        //Given
//        Player token = gameController.getToken();
//        gameController.setName(token.getToken(), "majs");
//        gameController.startGame(token.getToken());
//
//        //When
//        Game game = gameController.gameStatus(token.getToken());
//
//        //Then
//        assertEquals("OPEN", game.getGameStatus());
//        System.out.println("Game status= " + game.getGameStatus() + " is this.");
//    }
//
//    @Test
//    void check_game_status_for_both_player_success() throws UseException {
//        //Given
//        Player token = gameController.getToken();
//        Player joinedToken = gameController.getToken();
//        gameController.setName(token.getToken(), "Majs");
//        gameController.setName(joinedToken.getToken(), "Kolv");
//        Game game = gameController.startGame(token.getToken());
//        gameController.joinGame(joinedToken.getToken(), game.getId());
//
//        //When
//        Game joinedGame = gameController.gameStatus(joinedToken.getToken());
//        game = gameController.gameStatus(token.getToken());
//
//        //Then
//        assertEquals("Majs", game.getName());
//        assertEquals("Kolv", joinedGame.getName());
//        System.out.println("Joined game player name= " + joinedGame.getName() + " and opponent name= " + joinedGame.getOpponentName() + " is this.");
//        System.out.println("Started game player name= " + game.getName() + " and opponent name= " + game.getOpponentName() + " is this.");
//    }
//
//    @Test
//    void get_games_success() throws UseException {
//        //Given
//        Player token = gameController.getToken();
//        Player token2 = gameController.getToken();
//        gameController.setName(token.getToken(), "majs");
//        gameController.setName(token2.getToken(), "Kolv");
//        gameController.startGame(token.getToken());
//        gameController.startGame(token2.getToken());
//
//        //When
//        List<Game> games = gameController.gameList(token.getToken());
//
//        //Then
//        assertEquals(2, games.size());
//        System.out.println(games.size() + " games are here");
//    }
//
//    @Test
//    void make_move_success() throws UseException {
//        //Given
//        Player token = gameController.getToken();
//        gameController.setName(token.getToken(), "majs");
//        gameController.startGame(token.getToken());
//
//        //When
//        Game game = gameController.makeMove(token.getToken(), "rock");
//
//        //Then
//        assertNotNull(game.getMove());
//        assertNotNull(game.getName());
//        System.out.println("Move made: " + game.getMove() + " was this");
//    }
//
//    @Test
//    void get_game_result() throws UseException {
//        //Given
//        Player token = gameController.getToken();
//        Player token2 = gameController.getToken();
//        gameController.setName(token.getToken(), "majs");
//        gameController.setName(token2.getToken(), "Kolv");
//        Game game = gameController.startGame(token.getToken());
//        gameController.joinGame(token2.getToken(), game.getId());
//
//        //When
//        gameController.makeMove(token2.getToken(), "grth");
//        gameController.makeMove(token.getToken(), "PAPER");
//
//        //Then
//        Game joinedGame = gameController.gameStatus(token2.getToken());
//        assertEquals("WIN", joinedGame.getGameStatus());
//        System.out.println("Kolvs game status= " + joinedGame.getGameStatus() + " is this.");
//    }
//}
